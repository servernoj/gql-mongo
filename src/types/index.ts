import { Document } from 'mongoose'
import {
  GraphQLResolver,
  GraphQLResolvers
} from './base'

// Specific GraphQL resolvers
export type UserResolver<Result> = GraphQLResolver<Result, User>
export type UserResolvers = GraphQLResolvers & {
  User: {
    defaultItem: UserResolver<Item>,
    id: idResolver,
  },
  Item: {
    id: idResolver
  }
}

// Mongoose related types
export type idResolver = (doc: Document) => Document['_id']
export type Item = Document & {
  label: string,
}
export type User = Document & {
  items: [Item],
  defaultItem: Item['_id'],
  name: string,
  email: string
}


