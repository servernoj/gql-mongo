import { Query } from 'mongoose'
import { GraphQLResolveInfo } from 'graphql'

export type GraphQLResolver<Result, Parent> = (
  rootValue?: Parent,
  args?: any,
  context?: any,
  info?: GraphQLResolveInfo
) => Result | Promise<Result> | Query<Result> | undefined

export type GraphQLResolvers = {
  [key in 'Query' | 'Mutation']: {
    [key: string]: GraphQLResolver<any, any>;
  }
}

