import dotenv from 'dotenv'
dotenv.config()

import mongoose from 'mongoose';
import { ApolloServer } from 'apollo-server'
import apolloConfig from './apollo';

const { MONGO_HOST, MONGO_PORT, MONGO_INITDB_DATABASE, MONGO_INITDB_ROOT_USERNAME, MONGO_INITDB_ROOT_PASSWORD } = process.env

if (!MONGO_INITDB_ROOT_USERNAME || !MONGO_INITDB_ROOT_PASSWORD) {
  throw new Error(`Missing MONGO_INITDB_ROOT_USERNAME | MONGO_INITDB_ROOT_PASSWORD in environment variables`)
}
const mongoUri = `mongodb://${MONGO_HOST}:${MONGO_PORT}/?authSource=admin`
mongoose.connect(mongoUri, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,
  autoReconnect: true,
  user: MONGO_INITDB_ROOT_USERNAME,
  pass: MONGO_INITDB_ROOT_PASSWORD,
  dbName: MONGO_INITDB_DATABASE
})
  .catch(
    err => {
      console.error(err.message)
      process.exit(1)
    }
  )
  .then(() => {
    console.log('MongoDB server is running')
  })
  .then(() => {
    // Create API server
    const server = new ApolloServer(apolloConfig)
    // Start API server
    server.listen().then(({ url }) => {
      console.log(`🚀 API Server ready at ${url}`)
    })
  })
