import { gql } from 'apollo-server'

// Schema
export const typeDefs = gql`
  extend type Query {
    "A simple type for getting started!"
    hello: String
  }
`
// Resolvers
export const resolvers = {
  Query: {
    hello: () => 'worldddd'
  }
}

