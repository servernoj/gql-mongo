import mongoose from 'mongoose';
import { User } from '../../types'
const { Schema } = mongoose

const itemSchema = new Schema({
  label: String
})

const userSchema = new Schema({
  items: [itemSchema],
  defaultItem: Schema.Types.ObjectId,
  name: String,
  email: String
})
.index({
  email: 1
})

export const UserModel = mongoose.model<User>('User', userSchema)
