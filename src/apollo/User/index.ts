import { gql, UserInputError } from 'apollo-server'
import { UserModel } from './models'
import {
  UserResolvers,
} from '../../types'
import { getId } from '../../helpers'
import { ObjectId } from 'mongodb'



const getUserById = (userId: String) => {
  if (!userId) {
    throw new UserInputError(`Header 'authorization' is missing`)
  }
  return UserModel.findById(userId).then(user => {
    if (!user) {
      throw new UserInputError(`User ${userId} not found`)
    }
  })
}

// Schema
export const typeDefs = gql`
  extend type Query {
    getUsers: [User!]
    getUser: User
  }
  extend type Mutation {
    addUser(input: UserInput!): User!
    removeUser: User
    addItems(input: [ItemInput!]): User!
    removeItems(ids: [ID!]): User!
    setDefaultItem(id: ID!): User!
  }
  type Item {
    _id: String
    id: String,
    label: String
  }
  type User {
    _id: String
    id: String
    name: String!
    email: String!
    items: [Item!]
    defaultItem: Item
  }
  input UserInput {
    name: String!
    email: String!
  }
  input ItemInput {
    label: String!
  }
`

// Resolvers
export const resolvers: UserResolvers = {
  Item: {
    id: getId
  },
  User: {
    id: getId,
    defaultItem: user => {
      let item
      if (user) {
        item = user.items.find(e => e._id.equals(user.defaultItem))
      }
      return item
    }
  },
  Query: {
    getUsers() {
      return UserModel.find()
    },
    getUser(_, __, { userId }) {
      return UserModel.findById(userId)
    }
  },
  Mutation: {
    addUser(_, { input }) {
      const { email } = input
      return UserModel.findOne({ email }).then(user => {
        if (user) {
          throw new UserInputError(`User with email ${email} already exists`)
        }
        return new UserModel(input).save()
      })
    },
    removeUser(_, __, { userId }) {
      return getUserById(userId).then(_user => UserModel.findByIdAndDelete(userId))
    },
    addItems(_, { input }, { userId }) {
      return getUserById(userId)
        .then(_user => {
          return UserModel.findByIdAndUpdate(userId, {
            $push: {
              items: input
            }
          }, {
              new: true
            })
        })
    },
    removeItems(_, { ids }, { userId }) {
      return getUserById(userId)
        .then(_user => {
          return UserModel.findByIdAndUpdate(userId, {
            $pull: {
              items: {
                _id: {
                  $in: ids
                }
              }
            }
          }, {
              new: true
            })
        })
    },
    setDefaultItem(_, { id }, { userId }) {
      return getUserById(userId)
        .then(_user => {
          return UserModel.findByIdAndUpdate(userId, {
            $set: {
              defaultItem: new ObjectId(id)
            }
          }, {
              new: true
            })
        })
    }
  }
}
