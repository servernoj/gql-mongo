import { gql } from 'apollo-server'
import merge from 'lodash/merge'
import get from 'lodash/get'
import {
  typeDefs as HelloTypeDefs,
  resolvers as HelloResolvers
} from './Hello'
import {
  typeDefs as UserTypeDefs,
  resolvers as UserResolvers
} from './User'

const dummy = gql`
  type Query {
    dummy: String
  },
  type Mutation {
    dummy: String
  }
`

const typeDefs = [dummy, HelloTypeDefs, UserTypeDefs]
const resolvers = merge(HelloResolvers, UserResolvers)

export default {
  typeDefs,
  resolvers,
  context: (ctx: { req: any }) => {
    const userId = get(ctx, 'req.headers.authorization')
    return {
      userId
    }
  }
}
