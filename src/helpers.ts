import { idResolver } from './types' 
export const getId : idResolver = doc => doc._id.toHexString()